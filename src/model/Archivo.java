package model;

import java.util.ArrayList;
import java.util.List;

public class Archivo {
	List<Clase> clases;
	String path;
	
	public Archivo(String path){
		this.path = path;
		this.clases = new ArrayList<Clase>();
	}
	
	public List<Clase> getClases() {
		return clases;
	}
	public void setClases(List<Clase> clases) {
		this.clases = clases;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		String clasesString = "[";
		for( Clase c : clases ){
			clasesString += c + ", ";
		}
		clasesString += "]";
		return "Archivo [clases=" + clasesString + ", path=" + path + "]";
	}
}
