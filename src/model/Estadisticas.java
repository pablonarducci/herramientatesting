package model;

public class Estadisticas {
	Integer fanIn;
	Integer fanOut;
	Integer complejidad;
	Double porcentajeComentarios;
	public Integer getFanIn() {
		return fanIn;
	}
	public void setFanIn(Integer fanIn) {
		this.fanIn = fanIn;
	}
	public Integer getFanOut() {
		return fanOut;
	}
	public void setFanOut(Integer fanOut) {
		this.fanOut = fanOut;
	}
	public Integer getComplejidad() {
		return complejidad;
	}
	public void setComplejidad(Integer complejidad) {
		this.complejidad = complejidad;
	}
	public Double getPorcentajeComentarios() {
		return porcentajeComentarios;
	}
	public void setPorcentajeComentarios(Double porcentajeComentarios) {
		this.porcentajeComentarios = porcentajeComentarios;
	}
	@Override
	public String toString() {
		return "Estadisticas [fanIn=" + fanIn + ", fanOut=" + fanOut
				+ ", complejidad=" + complejidad + ", porcentajeComentarios="
				+ porcentajeComentarios + "]";
	}
	
}
