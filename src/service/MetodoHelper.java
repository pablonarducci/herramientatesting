package service;

import java.io.IOException;

import model.Codigo;
import model.Comentario;
import model.Metodo;
import model.Token;

public class MetodoHelper {
	Codigo codigoClase;
	char[] arrayCodigo;
	int nextIndex;
	
	
	private static final int NO_METODO = 0; //Aún no entré en el método
	private static final int PRE_METODO = 1; //Estoy en la línea de definición
	private static final int IN_METODO = 2; //Estoy dentro del método
	
	public MetodoHelper(String codigo) {
		this.codigoClase = new Codigo(codigo);
		this.arrayCodigo = codigo.toCharArray();
		this.nextIndex = 0;
	}

	public Metodo getNextMetodo() {
		Metodo metodo = new Metodo();
		int inMetodo = MetodoHelper.NO_METODO;
		String nombre = null;
		int parentesis = 0;
		int llaves = 0;
		Token siguientePalabra = null;
		int lastFinalizer = this.nextIndex;
		while( (siguientePalabra = this.siguientePalabra()) != null ){
			if( siguientePalabra.getTipo() == Token.TIPO_COMENTARIO ){
				continue;
			}
			if( inMetodo == MetodoHelper.NO_METODO ){
				//Previo a encontrar el nombre del método
				if( siguientePalabra.getTipo() == Token.TIPO_LLAVE_CERRADA || siguientePalabra.getValor().trim().equals(";") ){
					lastFinalizer = this.nextIndex;
				}
				if( siguientePalabra.getTipo() == Token.TIPO_NOMBRE ){
					nombre = siguientePalabra.getValor();
				}else if(nombre != null && siguientePalabra.getTipo() == Token.TIPO_PARENTESIS_ABIERTA ){
					metodo.setNombre(nombre);
					metodo.setFirstLine(lastFinalizer);
					inMetodo = MetodoHelper.PRE_METODO;
					parentesis ++;
					nombre = null;
				}else{
					nombre = null;
				}
			}else if( inMetodo == MetodoHelper.PRE_METODO ){
				//Dentro de la definición del método -- Ya encontró nombre (
				if( siguientePalabra.getTipo() == Token.TIPO_PARENTESIS_ABIERTA ){
					parentesis ++;
				}else if( siguientePalabra.getTipo() == Token.TIPO_PARENTESIS_CERRADA ){
					parentesis --;
				}else if( parentesis == 0 && siguientePalabra.getTipo() == Token.TIPO_LLAVE_ABIERTA ){
					inMetodo = MetodoHelper.IN_METODO;
					llaves ++;
				}
			}else{
				//Dentro del método
				if( siguientePalabra.getTipo() == Token.TIPO_LLAVE_ABIERTA ){
					llaves ++;
				}
				if( siguientePalabra.getTipo() == Token.TIPO_LLAVE_CERRADA ){
					llaves --;
					if( llaves ==  0 ){
						metodo.setLastLine(this.nextIndex);
						metodo.setCodigo(new Codigo(codigoClase.getCodigo().substring(metodo.getFirstLine(), metodo.getLastLine() + 1)));
						return metodo; // RETURN
					}
				} else if(  siguientePalabra.getTipo() == Token.TIPO_NOMBRE ){
					nombre = siguientePalabra.getValor();
				}else if(nombre != null && siguientePalabra.getTipo() == Token.TIPO_PARENTESIS_ABIERTA ){
					if( !CodigoService.esControladorDeFlujo(nombre) ){
						metodo.getMetodosLlamados().add(nombre);
					}
					nombre = null;
				}
			}
		}
		return null;
	}

	public Token siguientePalabra() {
		Character ultimoLeido = null;
		Comentario comentario = new Comentario();
		StringBuilder sb = new StringBuilder();
		for( ; this.nextIndex < this.arrayCodigo.length ; this.nextIndex++){
			if( ultimoLeido != null ){
				//Si no estoy en un comentario y no se reconoció '/*' o '//', lo que está adentro vale
				if( !comentario.isInComentario() && !ultimoLeido.equals('/') || ( this.actual() != '/' && this.actual() != '*' ) ){
					if( MetodoHelper.noEsLetra( this.actual() ) ){
						
						if( !isBlankString(sb.toString()) ){
							return new Token(sb.toString().trim());
						}
					}
				}else if( !comentario.isInComentario() ){
					//Si no es comentario quiere decir que se reconoció un comienzo de comentario (Sino hubiera entrado en el if anterior)
					if( this.actual() == '/' ){
						comentario.setIntoSimpleComment(true);
					}else{ //Por descarte viene un asterisco
						comentario.setIntoMultipleComment(true);
					}
				}else if(comentario.isIntoSimpleComment() && this.actual() == '\n' ){
					return new Token(sb.toString());
				}else if(comentario.isIntoMultipleComment() && ultimoLeido.equals('*') && this.actual() == '/' ){
					return new Token(sb.toString());
				}
			}else if( MetodoHelper.noEsLetra( this.actual() ) && !isBlankString("" + this.actual())){
				String actual = this.actual() + "";
				this.nextIndex ++;
				return new Token( actual );
			}
			ultimoLeido = this.actual();
			sb.append(this.actual());
		}
		return null;
	}
	
	public Token siguientePalabraSinComentario(){
		Token palabra = siguientePalabra();
		while( palabra != null && palabra.getTipo() == Token.TIPO_COMENTARIO ){
			palabra = siguientePalabra();
		}
		return palabra;
	}
	
	public Token primeraPalabra() {
		Token palabra = siguientePalabraSinComentario();
		boolean reconocida = false;
		int parentesisAbiertos = 0;
		//Avanzo hasta llegar adentro del método
		while( !reconocida && palabra != null ){
			//El método tiene que tener un nombre
			while( palabra != null && palabra.getTipo() != Token.TIPO_NOMBRE ){
				palabra = siguientePalabraSinComentario();
			}
			if( palabra == null ){
				return null;
			}
			palabra = siguientePalabraSinComentario();

			if( palabra == null ){
				return null;
			}
			
			//Tiene que seguir con un paréntesis abierto
			if( palabra.getTipo() != Token.TIPO_PARENTESIS_ABIERTA ){
				continue;
			}
			palabra = siguientePalabraSinComentario();
			parentesisAbiertos = 1;

			if( palabra == null ){
				return null;
			}
			
			while( !reconocida && palabra != null ){
				if( palabra.getTipo() == Token.TIPO_PARENTESIS_ABIERTA ){
					parentesisAbiertos ++;
				}
				if( palabra.getTipo() == Token.TIPO_PARENTESIS_CERRADA ){
					parentesisAbiertos --;
					if( parentesisAbiertos == 0 ){
						reconocida = true;
					}
				}
				palabra = siguientePalabra();
			}

			if( palabra == null ){
				return null;
			}
		}
		return palabra;
	}
	
	private static boolean noEsLetra(char actual) { 
		return !Character.isLetterOrDigit(actual) && actual != '_' && actual != '-';
	}

	private char actual(){
		return this.arrayCodigo[this.nextIndex];
	}
	public static boolean isBlankString(String s){
		return s.trim().isEmpty() || s.trim().equals(" ") || s.trim().equals("\t") || s.trim().equals("\n");
	}
	public static void main (String[] args ) throws IOException{
		String texto = CodigoService.readFile("/home/pablo/testFolder/ClaseDePrueba.java");
		MetodoHelper ch = new MetodoHelper(texto);
		System.out.println(ch.primeraPalabra().getValor());
		/*
		while( (m = ch.getNextMetodo() ) != null ){
			System.out.println(m.toString());
			System.out.println(texto.substring(m.getFirstLine(), m.getLastLine() + 1 ));
		}
		*/
	}
}
