package service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;

import model.Codigo;
import model.Comentario;
import model.Token;

public class CodigoService {
	
	public static final String[] controladoresDeFlujo = {"if", "switch","while", "for"};

	public static Codigo sacarComentarios(Codigo codigo){
		Codigo codigoPodado = new Codigo();
		Comentario comentario = new Comentario();
		Character ultimoLeido = null;
		for( Character c : codigo.toCharArray() ){
			if( ultimoLeido != null ){
				if( !comentario.isInComentario() && !ultimoLeido.equals('/') ){
					codigoPodado.getSb().append(ultimoLeido);
				}else if( !comentario.isInComentario() ){
					if( c.equals('/') ){
						comentario.setIntoSimpleComment(true);
					}else if( c.equals('*') ){
						comentario.setIntoMultipleComment(true);
					}else{
						codigoPodado.getSb().append(ultimoLeido);
					}
				}else if(comentario.isIntoSimpleComment() && c.equals('\n') ){
					comentario.setIntoSimpleComment(false);
				}else if(comentario.isIntoMultipleComment() && ultimoLeido.equals('*') && c.equals('/') ){
					comentario.setIntoMultipleComment(false);
					c = null;
				}
			}
			ultimoLeido = c;
		}
		codigoPodado.buildCode();
		return codigoPodado;
	}
	
	public static Double calcularPorcentajeComantarios(Codigo codigo){
		Comentario comentario = new Comentario();
		Character ultimoLeido = null;
		Integer lineasCodigo = 0;
		Integer lineasComentarios = 0;
		boolean huboCodigo = false;
		boolean huboComentarios = false;
		for( Character c : codigo.toCharArray() ){
			if( ultimoLeido != null ){
				if( !comentario.isInComentario() && !ultimoLeido.equals('/')){
					if( ! new Character('/').equals(c) ){
						huboCodigo = true;
					}
				}else if( !comentario.isInComentario() ){
					if( c.equals('/') ){
						comentario.setIntoSimpleComment(true);
						huboComentarios = true;
					}else if( c.equals('*') ){
						comentario.setIntoMultipleComment(true);
						huboComentarios = true;
					}else{
						huboCodigo = true;
					}
				}else if(comentario.isIntoSimpleComment() && c.equals('\n') ){
					comentario.setIntoSimpleComment(false);
				}else if(comentario.isIntoMultipleComment() && ultimoLeido.equals('*') && c.equals('/') ){
					comentario.setIntoMultipleComment(false);
					huboComentarios = true;
					c = null;
				}else if( comentario.isInComentario() ){
					huboComentarios = true;
				}
			}
			if( new Character('\n').equals(c) ){
				if( huboComentarios ){
					lineasComentarios ++;
				}
				if( huboCodigo ){
					lineasCodigo ++;
				}
				huboCodigo = false;
				huboComentarios = false;
			}
			ultimoLeido = c;
		}
		return  (double) lineasComentarios  * 100.0 / ( (double) lineasCodigo  + (double) lineasComentarios );
	}
	
	/*
	 * En principio va a contar las cláusulas
	 * 		1) IF
	 * 		2) ELSE IF
	 * 		3) CASE
	 * 		4) WHILE
	 * 		5) FOR 
	 */
	public static Integer calcularComplejidadCiclomatica(Codigo metodo){
		MetodoHelper mh = new MetodoHelper( metodo.getCodigo() );
		Token palabra = mh.primeraPalabra();
		int complejidad = 1;
		while( palabra != null ){
			if( CodigoService.esControladorDeFlujo(palabra.getValor()) ){
				complejidad ++;
			}
			palabra = mh.siguientePalabraSinComentario();
		}
		return complejidad;
	}
	
	public static boolean esControladorDeFlujo(String palabra){
		palabra.trim();
		for( String s : controladoresDeFlujo ){
			if( s.equals(palabra) ){
				return true;
			}
		}
		return false;
	}
	
	public static String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
	public static void main( String[] args ) throws IOException{
		String texto = readFile("/home/pablo/testFolder/ClaseDePrueba.java");
		Codigo c = new Codigo(texto);
		Codigo podado = sacarComentarios(c);
		System.out.println(podado.getCodigo());
		System.out.println("---- > VERSUS < ------");
		System.out.println(c.getCodigo());
		
		Double porcentaje = calcularPorcentajeComantarios(c);
		DecimalFormat df = new DecimalFormat("0.00");
		System.out.println("Porcentaje = " + df.format(porcentaje) + "%");
		
		int complejidad = calcularComplejidadCiclomatica(c);
		System.out.println("Complejidad = " + complejidad );
	}
}
